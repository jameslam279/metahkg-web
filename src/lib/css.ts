export const colors = {
    link: "#3498db",
    blue: "#34aadc",
    grey: "#aca9a9",
    yellow: "#f5bd1f",
};

export const css = {
    link: `text-[${colors.link}] !no-underline hover:!underline cursor-pointer`,
    svgwhite: "invert sepia-0 saturate-0 hue-rotate-[93deg] !brightness-[103%]",
    smallBtn:
        "!rounded-[5px] !m-0 !text-metahkg-grey !p-0 !pl-[10px] !pr-[10px] !pt-[3px] !pb-[3px] !min-w-0",
};
